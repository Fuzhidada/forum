package com.fuzhi.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import org.apache.catalina.connector.Request;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fuzhi.beans.Freply;
import com.fuzhi.beans.Sreply;
import com.fuzhi.beans.Topic;
import com.fuzhi.beans.Users;
import com.fuzhi.services.MainService;
import com.fuzhi.services.SingleService;
import com.fuzhi.services.UserService;

@Controller
public class MainController {
	@Resource
	private MainService mainBus;
	@Resource
	private UserService userBus;
	@Resource
	private SingleService sinBus;

	@RequestMapping(value = "/main.html")
	public String remain(@RequestParam("topic") String topic,
			@ModelAttribute(value = "topics") HashMap<Topic, Users> map, Model model) {
		model.addAttribute("topic", map);
		return "main";
	}

	@ModelAttribute(value = "topics") // 准备main的topic
	public HashMap<Topic, Users> getTopics(HttpSession ses) {
		if (ses.getAttribute("num") == null) {
			ses.setAttribute("num", 20);
		} else {
			ses.setAttribute("num", Integer.parseInt(ses.getAttribute("num").toString()) + 5);
		}

		List<Topic> lis = mainBus.getTopic(Integer.parseInt(ses.getAttribute("num").toString()));

		HashMap<Topic, Users> map = new HashMap<>();
		for (int i = 0; i < lis.size(); i++) {
			Users u = userBus.selectUser(lis.get(i).getUserid());
			Topic t = lis.get(i);
			map.put(t, u);
		}
		return map;
	}

	@RequestMapping(value = "/single.html") // 跳转single页面加载所有信息 抽离一二级回复 modified
											// 2018-05-18 // 将一级二级回复抽离出来
	public String toSingle(@RequestParam("singleTopic") int titleid, Model m, HttpSession s) {
		Topic tt = mainBus.gettopicByid(titleid);
		Users uu = userBus.selectUser(tt.getUserid());
		HashMap<Topic, Users> mms = new HashMap<>();
		mms.put(tt, uu);
		m.addAttribute("maintopic", mms); // 楼主的
		m.addAttribute("fsreply", fsreplys(titleid, s, tt.getTopicid())); // 一二级回复
		return "single";
	}

	@ResponseBody // 下滑刷新 一级回复
	@RequestMapping(value = "/single1.html") // 跳转single页面加载所有信息 抽离一二级回复
												// modified 2018-05-18// //
												// 将一级二级回复抽离出来
	public LinkedHashMap<HashMap, ArrayList<HashMap>> toSingle1(@RequestParam("singleTopic") int titleid, Model m,
			HttpSession s) throws Exception {
		LinkedHashMap<HashMap, ArrayList<HashMap>> linkmap = new LinkedHashMap<>();
		Topic tt = mainBus.gettopicByid(titleid);
		linkmap = fsreply(titleid, s, tt.getTopicid()); // 一二级回复
		return linkmap;
	}

	// 公共方法 每次加载5个topic
	public LinkedHashMap<HashMap, ArrayList<HashMap>> fsreplys(int titleid, HttpSession ssion, int topicid) {
		HashMap mms = (HashMap) ssion.getAttribute("num1");
		LinkedHashMap<HashMap, ArrayList<HashMap>> linkmap = new LinkedHashMap<>();
		int count = sinBus.getfloor(topicid);

		if (mms == null) {
			if (count >= 5) {
				ssion.setAttribute("counts", count);
				ArrayList<HashMap> freply = sinBus.selectallFFs(titleid, 5); // 一级回复

				for (int i = 0; i < freply.size(); i++) { // 二级回复
					ArrayList<HashMap> sreply = sinBus.selectallSS((Long) freply.get(i).get("freplyid"));
					linkmap.put(freply.get(i), sreply);
				}
				return linkmap;
			} else {
				ArrayList<HashMap> freply = sinBus.selectallFFs(titleid, count); // 一级回复
				ssion.setAttribute("counts", count); // 将小于5的topic 数量放在session中
														// 以免用户新增回复后下滑加载不到
				for (int i = 0; i < freply.size(); i++) { // 二级回复
					ArrayList<HashMap> sreply = sinBus.selectallSS((Long) freply.get(i).get("freplyid"));
					linkmap.put(freply.get(i), sreply);
				}

				if (linkmap.isEmpty()) { // 若无更多回复 ，则不做处理
					return linkmap;
				} else {
				}
				return linkmap;
			}

		} else {
			if (mms.containsKey(titleid)) {

				if (count >= 5) {
					ArrayList<HashMap> freply = sinBus.selectallFFs(titleid,
							5 + Integer.parseInt(((HashMap) ssion.getAttribute("num1")).get(titleid).toString())); // 一级回复

					for (int i = 0; i < freply.size(); i++) { // 二级回复
						ArrayList<HashMap> sreply = sinBus.selectallSS((Long) freply.get(i).get("freplyid"));
						linkmap.put(freply.get(i), sreply);
					}
					return linkmap;
				} else {
					ArrayList<HashMap> freply = sinBus.selectallFFs(titleid, count); // 一级回复
					ssion.setAttribute("counts", count);// 将小于5的topic
														// 数量放在session中
														// 以免用户新增回复后下滑加载不到
					for (int i = 0; i < freply.size(); i++) { // 二级回复
						ArrayList<HashMap> sreply = sinBus.selectallSS((Long) freply.get(i).get("freplyid"));
						linkmap.put(freply.get(i), sreply);
					}

					if (linkmap.isEmpty()) { // 若无更多回复 ，则不做处理
						return linkmap;
					} else {
					}
					return linkmap;
				}

			}
		}
		return linkmap;
	}

	// 公共方法 每次加载5个topic //一级回复公共方法
	public LinkedHashMap<HashMap, ArrayList<HashMap>> fsreply(int titleid, HttpSession ssion, int topicid) {
		HashMap<Object, Object> mm = new HashMap<>();
		HashMap mms = (HashMap) ssion.getAttribute("num1");
		LinkedHashMap<HashMap, ArrayList<HashMap>> linkmap = new LinkedHashMap<>();
		int counts = Integer.parseInt(ssion.getAttribute("counts").toString());
		int count = sinBus.getfloor(topicid);

		if (mms == null) {
			if (count > 5) {
				mm.put(titleid, 5);
				ssion.setAttribute("num1", mm);
			} else {
				mm.put(titleid, counts + 1);
				ssion.setAttribute("num1", mm);
			}

		} else {
			if (mms.containsKey(titleid)) {

				int sessnun = Integer.parseInt(((HashMap) ssion.getAttribute("num1")).get(titleid).toString());
				if (count - sessnun > 0) {
					int sessnun1 = Integer.parseInt(((HashMap) ssion.getAttribute("num1")).get(titleid).toString());
					ArrayList<HashMap> freply = sinBus.selectallFF(titleid, sessnun1); // 一级回复
					for (int i = 0; i < freply.size(); i++) { // 二级回复
						ArrayList<HashMap> sreply = sinBus.selectallSS((Long) freply.get(i).get("freplyid"));
						linkmap.put(freply.get(i), sreply);
					}
					mm.put(titleid, sessnun + 1);
					ssion.setAttribute("num1", mm);

					return linkmap;

				} else {
					if (count <= 5) {
						if (count > counts) {
							mm.put(titleid, counts + 1);
							ssion.setAttribute("num1", mm);
						}
					}
				}
			} else { // 更换帖子hou 将页码的session 变为0
				mm.put(titleid, 5);
				ssion.setAttribute("num1", mm);
				return linkmap; // 直接返回空集合
			}

		}

		int sessnun1 = Integer.parseInt(((HashMap) ssion.getAttribute("num1")).get(titleid).toString());
		ArrayList<HashMap> freply = sinBus.selectallFF(titleid, sessnun1); // 一级回复
		for (int i = 0; i < freply.size(); i++) { // 二级回复
			ArrayList<HashMap> sreply = sinBus.selectallSS((Long) freply.get(i).get("freplyid"));
			linkmap.put(freply.get(i), sreply);
		}
		return linkmap;
	}

	// 回一级回复
	@Transactional
	@RequestMapping(value = "/relouzhu.html")
	@ResponseBody
	public String relouzhu(@RequestParam("content") String content, @RequestParam("topicid") Integer topicid,
			HttpSession s) {
		try {
			Freply f = new Freply();
			f.setContent(content);
			f.setDate(new Date());
			f.setUserid(((Users) s.getAttribute("users")).getUserid());
			f.setTopicid(topicid);
			f.setFloor(sinBus.getfloor(topicid) + 1);
			f.setGood(0);
			int result = sinBus.insertone(f);
			return content;

		} catch (Exception e) {
			return "error_1996";
		}
	}

	@Transactional
	@RequestMapping(value = "/refreply.html")
	@ResponseBody
	public String reFreply(@RequestParam("content") String content, @RequestParam("freplyid") Integer freplyid,
			HttpSession ss) {
		try {
			Sreply s = new Sreply();
			s.setContent(content);
			s.setDate(new Date());
			s.setUserid(((Users) ss.getAttribute("users")).getUserid());
			s.setFreplyid(freplyid);
			s.setFloor(sinBus.getfloor1(freplyid) + 1);
			s.setGood(0);
			int result = sinBus.insertone1(s);
			return content;
		} catch (Exception e) {
			return "error_1996";
		}
	}

	@RequestMapping(value = "/fatopic.html")
	public String topic(@RequestParam("title") String title, @RequestParam("content") String content, HttpSession ss) {
		try {

			Topic t = new Topic();
			t.setTitle(title);
			t.setContent(content);
			t.setDate(new Date());
			t.setUserid(((Users) ss.getAttribute("users")).getUserid());
			t.setGood(0);

			int a = mainBus.insertone(t);
		} catch (Exception e) {
			return "index";
		}

		return "redirect:index.jsp";
	}

}

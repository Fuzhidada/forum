package com.fuzhi.controllers;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fuzhi.beans.Users;
import com.fuzhi.services.UserService;

@Controller
public class UserContorller {
	@Resource
	private UserService userBus;

	@RequestMapping(value = "/login.html")
	public String a(@RequestParam("username") String username, @RequestParam("password") String password,
			HttpSession session) {
		Users u = new Users();
		u.setUsername(username);
		u.setPassword(password);
		System.out.println(username);
		Users result = userBus.selectLogin(u);

		if (result != null) {
			session.setAttribute("users", result);
		}

		return "redirect:index.jsp";
	}
}

package com.fuzhi.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatDate {
	public String format(String string) {
		SimpleDateFormat sim = new SimpleDateFormat("yyyy-mm-dd");
		return sim.format(string);
	}
}

package com.fuzhi.servicesimp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuzhi.beans.Topic;
import com.fuzhi.daos.TopicMapper;
import com.fuzhi.services.MainService;

@Service("mainBus")
public class MainServiceImp implements MainService {

	@Autowired
	private TopicMapper dd;

	public List<Topic> getTopic(int num) {
		return (List<Topic>) dd.selectall(num);
	}

	@Override
	public Topic gettopicByid(int id) {
		return dd.selectone(id);
	}

	@Override
	public int insertone(Topic t) {
		// TODO Auto-generated method stub
		return dd.insertone(t);
	}
}

package com.fuzhi.servicesimp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuzhi.beans.Users;
import com.fuzhi.daos.UsersMapper;
import com.fuzhi.services.UserService;

@Service("userBus")
public class UserServiceImp implements UserService {
	@Autowired
	private UsersMapper dd;

	// 登录验证

	public Users selectLogin(Users u) {
		System.out.println();
		Users xx = dd.selectLogin(u);
		return xx;
	}

	@Override
	public Users selectUser(int id) {
		return dd.selectUser(id);
	}
}

package com.fuzhi.servicesimp;

import java.awt.List;
import java.util.ArrayList;
import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fuzhi.beans.Freply;
import com.fuzhi.beans.Sreply;
import com.fuzhi.daos.FreplyMapper;
import com.fuzhi.daos.SreplyMapper;
import com.fuzhi.services.SingleService;

@Service("sinBus")
public class SingleServiceImp implements SingleService {
	@Autowired
	private FreplyMapper ff;

	@Autowired
	private SreplyMapper ss;

	@Override
	public ArrayList<HashMap> selectallFF(int id, int num1) {

		return (ArrayList) ff.selectAll(id, num1);
	}

	public ArrayList<HashMap> selectallFFs(int id, int num1) {

		return (ArrayList) ff.selectAlls(id, num1);
	}

	@Override
	public ArrayList<HashMap> selectallSS(Long id) {

		return ss.selectAll(id);
	}

	@Override
	public int insertone(Freply f) {

		return ff.insertone(f);
	}

	@Override
	public int getfloor(int topicid) {
		// TODO Auto-generated method stub
		return ff.getfloor(topicid);
	}

	@Override
	public int getfloor1(int freplyid) {
		// TODO Auto-generated method stub
		return ss.getfloor1(freplyid);
	}

	@Override
	public int insertone1(Sreply s) {
		// TODO Auto-generated method stub
		return ss.insertone1(s);
	}

}

package com.fuzhi.services;

import java.util.List;

import com.fuzhi.beans.Topic;

public interface MainService {
	List<Topic> getTopic(int num);

	Topic gettopicByid(int id);

	int insertone(Topic t);
}

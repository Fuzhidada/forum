package com.fuzhi.services;

import com.fuzhi.beans.Users;

public interface UserService {
	public Users selectLogin(Users u);

	public Users selectUser(int id);
}

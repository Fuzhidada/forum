package com.fuzhi.daos;

import org.springframework.stereotype.Repository;

import com.fuzhi.beans.Users;

@Repository
public interface UsersMapper {

	Users selectLogin(Users u);

	Users selectUser(int id);
}
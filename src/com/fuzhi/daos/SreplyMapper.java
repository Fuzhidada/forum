package com.fuzhi.daos;

import java.util.ArrayList;
import java.util.HashMap;

import com.fuzhi.beans.Sreply;

public interface SreplyMapper {
	ArrayList selectAll(long id);

	int deleteByPrimaryKey(Integer sreply);

	int insert(Sreply record);

	int insertSelective(Sreply record);

	int getfloor1(int freplyid);

	int insertone1(Sreply s);

	Sreply selectByPrimaryKey(Integer sreply);

	int updateByPrimaryKeySelective(Sreply record);

	int updateByPrimaryKey(Sreply record);
}
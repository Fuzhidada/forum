package com.fuzhi.daos;

import java.util.List;

import com.fuzhi.beans.Topic;

public interface TopicMapper {
	List<Topic> selectall(Integer num);

	Topic selectone(Integer id);

	int insertone(Topic t);

}
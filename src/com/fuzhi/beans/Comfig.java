package com.fuzhi.beans;

public class Comfig {
	/**
	 *
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column comfig.js
	 *
	 * @mbg.generated Thu Mar 29 15:37:15 CST 2018
	 */
	private String js;

	/**
	 * This method was generated by MyBatis Generator. This method returns the
	 * value of the database column comfig.js
	 *
	 * @return the value of comfig.js
	 *
	 * @mbg.generated Thu Mar 29 15:37:15 CST 2018
	 */
	public String getJs() {
		return js;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the
	 * value of the database column comfig.js
	 *
	 * @param js
	 *            the value for comfig.js
	 *
	 * @mbg.generated Thu Mar 29 15:37:15 CST 2018
	 */
	public void setJs(String js) {
		this.js = js == null ? null : js.trim();
	}
}
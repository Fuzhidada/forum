<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<% String path=request.getContextPath() ;
  String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<base href="<%=basePath %>">
<head>
<title>forum-login&register</title>
<meta charset="UTF-8" />

<script type="text/javascript" src="resources/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="resources/js/login.js"></script>


<link rel="stylesheet" type="text/css" href="resources/css/login.css" />


</head>

<body style="margin: 0px;">
	<img class='logo' src="resources/img/login/1.png"></img>

	<form class='log1'>
		<div class='logincheck'>
			<a class="loginforpassword" href="">密码登录</a>&nbsp|&nbsp<a
				class="loginfortel" href="">手机登录</a>
		</div>

		<div class="rememberpass">
			记住密码<input type='checkbox'>
		</div>

		<input class='username' type="text" placeholder="请输入用户名"> <input
			class='password' type="password" placeholder="请输入密码">

		<p class='tip1'>必须以下划线或字母开头，长度为4-16位</p>
		<p class='tip2'>必须以下划线或字母开头，长度为6-16位</p>
		<div class='check1'>
			<input type='checkbox' id="agree1">阅读并同意《平台用户使用条款》| <a
				class="register" href="">注册账号</a>&nbsp|<a class='findpassword'
				href="">找回密码</a>
		</div>
		<input class='loginbut' type="button" value="登录">

	</form>


	<img class="nameimg" src="resources/img/login/nameimg.png"></img>
	<img class="passwordimg" src="resources/img/login/passwordimg.png"></img>






</body>
</html>
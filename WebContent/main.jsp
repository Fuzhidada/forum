<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt"%>
<% String path=request.getContextPath() ;
  String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<%=basePath %>">
<title>forum&main</title>

<script type="text/javascript" src="resources/js/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/index.css" />
<script type="text/javascript" src="resources/js/index.js"></script>
<link rel="shortcut icon" href='resources/img/fuzhi.ico'
	type='image/x-icon'>

<link rel="stylesheet" type="text/css" href="resources/css/main.css" />
<script type="text/javascript" src="resources/js/main.js"></script>
</head>

<body style="margin: 0px; background-image: url('resources/img/10.jpg')">
	<%@include file="head.jsp"%>


	<table class="table1" cellspacing=0px cellpadding=0px;>
		<tr>
			<td class="td1"><img src="resources/img/122.jpg" width="100%"
				height="45px" title="返回首页"></td>
			<td class="td2"><input class="inp1" type='text'></td>
			<td class="td3">搜一下</td>
		</tr>
	</table>

	<div class="div0">
		<div class="divx">

			<div class="div1">
				<div class="div11">精选专题</div>
				<div class="div12">精选专题</div>
				<div class="div13">精选专题</div>
				<div class="div14">精选专题</div>
				<div class="div15">精选专题</div>
				<div class="div16">精选专题</div>
				<div class="div17">精选专题</div>
				<div class="div18">精选专题</div>



			</div>

			<div class="div2">
				<div class="div21">
					<div class="div211">
						<img src="resources/img/s4.jpg" class="div211_img" />
						<p class="div211_img_p">实时热点</p>
						<div class="div211_img_p1">
							xxxxxxxxxxxx<br />xxxxxxxxxxxxx
						</div>
						<div class="div211_1_hot">
							<img src="resources/img/hot.jpg" class="div211_img_img1" /> <span
								class="hot_span">1235</span> <img src="resources/img/huifu.jpg"
								class="div211_img_img2"> <span class="hot_span">130</span>
						</div>
					</div>

					<div class="div212">
						<img src="resources/img/s3.jpg" class="div212_img" />
						<p class="div211_img_p">实时热点</p>
						<div class="div211_img_p1">
							xxxxxxxxxxx<br />xxxxxxxxxxxxx
						</div>
						<div class="div211_1_hot">
							<img src="resources/img/hot.jpg" class="div211_img_img1"> <span
								class="hot_span">250</span> <img src="resources/img/huifu.jpg"
								class="div211_img_img2"> <span class="hot_span">302</span>
						</div>
					</div>

					<div class="div213">
						<div class="div213_button1" ><img class="div213_button1_img" src="resources/img/left1.jpg"></div>
						<div class="div213_div">
							<img class="div213_img" src="resources/img/f1.jpg">
						</div>
						<div class="div213_div">
							<img class="div213_img" src="resources/img/f2.jpg">
						</div>
						<div class="div213_div">
							<img class="div213_img" src="resources/img/f3.jpg">
						</div>
						<div class="div213_div">
							<img class="div213_img" src="resources/img/f4.jpg">
						</div>
						<div class="div213_button2"><img class="div213_button2_img" src="resources/img/right1.png"></div>
					</div>

				</div>

				<div class="div22">

					<c:forEach var="map" items="${topic}">
						<div class="div22_main" tip="${map.key.topicid}">

							<div class="div22_title">${map.key.title}</div>
							<div class="div22_readnum">999333</div>
							<div class="div22_topic">军事</div>
							<div class="div22_user_message">
								<img class="div22_samllimg" src="${map.value.userimage}" /><a
									href="#" class="username">${map.value.username}</a>
							</div>
							<div class="div22_time">
								<f:formatDate value="${map.key.date}" pattern="yyyy-MM-dd " />
							</div>
							<div class="div22_replynum">
								<img src="resources/img/dianzan.png" class="div22_samllimg">
								<p class="renum">${map.key.good}
								<p>
							</div>
							<div class="div22_readp">阅读量</div>
						</div>
					</c:forEach>


				</div>
			</div>

			<!--start div3-->
			<div class="div3"></div>
			<!--end-->

		</div>
	</div>

	<p class="mark">&copy湖北文理学院&nbsp&nbsp&nbsp2014级本科毕业论文&nbsp指导老师：潘德林&nbsp答辩学生：付志</p>


</body>
</html>

/* Drop Tables */

DROP TABLE advertisement;
DROP TABLE comfig;
DROP TABLE sreply;
DROP TABLE freply;
DROP TABLE power;
DROP TABLE topic;
DROP TABLE user_message;
DROP TABLE users;




/* Create Tables */

CREATE TABLE advertisement
(
	advertisementid int unsigned NOT NULL UNIQUE AUTO_INCREMENT COMMENT 'advertisementid',
	state tinyint unsigned COMMENT 'state : 0������1.����',
	image varchar(200) COMMENT 'image',
	introduce varchar(500) COMMENT 'introduce',
	adverurl varchar(200) COMMENT 'adverurl',
	PRIMARY KEY (advertisementid)
) COMMENT = 'advertisement';


CREATE TABLE comfig
(
	js varchar(100) COMMENT 'js'
) COMMENT = 'comfig';


CREATE TABLE freply
(
	freplyid int unsigned NOT NULL AUTO_INCREMENT COMMENT 'freplyid',
	content varchar(500) NOT NULL COMMENT 'content',
	date date NOT NULL COMMENT 'date',
	userid int unsigned NOT NULL  COMMENT 'userid',
	topicid int unsigned NOT NULL  COMMENT 'topicid',
	floor int unsigned NOT NULL  COMMENT 'floor',
	good int unsigned DEFAULT 0 COMMENT 'good',
	PRIMARY KEY (freplyid)
) COMMENT = 'freply';


CREATE TABLE power
(
	userid int unsigned NOT NULL UNIQUE COMMENT 'userid',
	sysadd tinyint DEFAULT 1 COMMENT 'sysadd',
	sysdelete tinyint DEFAULT 0 COMMENT 'sysdelete',
	sysupdate tinyint DEFAULT 1 COMMENT 'sysupdate',
	sysselect tinyint DEFAULT 1 COMMENT 'sysselect'
) COMMENT = 'power';


CREATE TABLE sreply
(
	sreply int unsigned NOT NULL AUTO_INCREMENT COMMENT 'sreply',
	content varchar(500) NOT NULL COMMENT 'content',
	date date NOT NULL COMMENT 'date',
	userid int unsigned NOT NULL  COMMENT 'userid',
	freplyid int unsigned NOT NULL COMMENT 'freplyid',
	floor int unsigned NOT NULL  COMMENT 'floor',
	good int unsigned DEFAULT 0 NOT NULL COMMENT 'good',
	PRIMARY KEY (sreply)
) COMMENT = 'sreply';


CREATE TABLE topic
(
	topicid int unsigned NOT NULL UNIQUE AUTO_INCREMENT COMMENT 'topicid',
	title varchar(200) NOT NULL COMMENT 'title',
	content varchar(500) NOT NULL COMMENT 'content',
	date date NOT NULL COMMENT 'date',
	userid int unsigned NOT NULL  COMMENT 'userid',
	good int unsigned DEFAULT 0 COMMENT 'good',
	PRIMARY KEY (topicid)
) COMMENT = 'topic';


CREATE TABLE users
(
	userid int unsigned NOT NULL UNIQUE AUTO_INCREMENT COMMENT 'userid',
	username varchar(16) NOT NULL UNIQUE COMMENT 'username',
	password varchar(16) NOT NULL COMMENT 'password',
	userimage varchar(200) COMMENT 'userimage : ���ͷ���ַ',
	usersid int unsigned NOT NULL  COMMENT 'usersid',
	PRIMARY KEY (userid)
) COMMENT = 'users';


CREATE TABLE user_message
(
	sumtime int unsigned COMMENT 'sumtime : �ܵ�¼ʱ��',
	message_state int unsigned DEFAULT 0 NOT NULL COMMENT 'message_state',
	todaytime int unsigned DEFAULT 0 COMMENT 'todaytime',
	userid int unsigned NOT NULL  COMMENT 'userid'
) COMMENT = 'user_message';



/* Create Foreign Keys */

ALTER TABLE sreply
	ADD FOREIGN KEY (freplyid)
	REFERENCES freply (freplyid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE freply
	ADD FOREIGN KEY (topicid)
	REFERENCES topic (topicid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE freply
	ADD FOREIGN KEY (userid)
	REFERENCES users (userid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE power
	ADD FOREIGN KEY (userid)
	REFERENCES users (userid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE sreply
	ADD FOREIGN KEY (userid)
	REFERENCES users (userid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE topic
	ADD FOREIGN KEY (userid)
	REFERENCES users (userid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE users
	ADD FOREIGN KEY (userid)
	REFERENCES users (userid)
	ON UPDATE cascade
	ON DELETE cascade
;


ALTER TABLE user_message
	ADD FOREIGN KEY (usersid)
	REFERENCES users (userid)
	ON UPDATE cascade
	ON DELETE cascade
;




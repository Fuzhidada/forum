<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% String path=request.getContextPath() ;
  String basePath=request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head Cache-Control:no-cache>
<base href="<%=basePath %>">
<title>forum</title>
<script type="text/javascript" src="resources/js/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/index.css" />
<script type="text/javascript" src="resources/js/index.js"></script>
<link rel="shortcut icon" href='resources/img/fuzhi.ico'
	type='image/x-icon'>

<script type="text/javascript" src="resources/js/single.js"></script>
<link rel="stylesheet" type="text/css" href="resources/css/single.css" />


 <link href="ueditor/themes/default/css/umeditor.css" type="text/css" rel="stylesheet">
    <script type="text/javascript" src="ueditor/third-party/jquery.min.js"></script>
    <script type="text/javascript" charset="utf-8" src="ueditor/umeditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="ueditor/umeditor.min.js"></script>
    <script type="text/javascript" src="ueditor/lang/zh-cn/zh-cn.js"></script>

</head>
<body style="margin: 0px; background-image: url('resources/img/11.jpg')">
	<%@include file="head.jsp"%>
	

	<c:forEach items="${maintopic}" var="louzhu">
	<input type="hidden" value="${louzhu.key.topicid}" id="topicid">
	
	<div class="single_title0">
		<label class="single_title">${louzhu.key.title}</label>
	</div>

	<table cellpadding=0px cellspacing="0px" class="single_table">
		<!--楼主信息+ -->
		<tr>
			<td>楼主：</td>
			<td>${louzhu.value.username}</td>
			<td>时间：</td>
			<td><f:formatDate value="${louzhu.key.date}"  pattern="yyyy-MM-dd " /></td>
			<td>回复：</td>
			<td>100000</td>
		</tr>
	</table>

	<div class="single_div0">
		<!-- 总  div -->
		<div class="main_louzhu">

			<div class="main_content">
			${louzhu.key.content}
			</div>

			<div class="main_caozuo">
				<label class="caozuo1">楼主</label> <label class="louzhucaozuo21">评论</label>
				<label class="caozuo3">点赞</label> <label class="caozuo4">${louzhu.key.good}个赞</label>
			</div>
		</div>
</c:forEach>


		<pre>
			<!--控制主楼与 楼下的间隙  -->


</pre>

<c:forEach items="${fsreply}" var="fsreply">
		<div class="main_huifu" ffloor=${ fsreply.key.floor}>
			<div class="main_huifu_content">${fsreply.key.content}</div>

			<div class="main_hufuuser">
				<label class="zuozhe">作者:</label> <label class="username">${fsreply.key.username}</label>
				<label class="riqi">日期:</label> <label class="riqix"><f:formatDate value="${fsreply.key.date}" pattern="yyyy-MM-dd " />
				</label>
			</div>

			<div class="main_huifu_caozuo">
				<label class="caozuo1">${fsreply.key.floor}楼</label> <label class="louzhucaozuo22" freplyid="${fsreply.key.freplyid}">回复(66)</label>
				<label class="caozuo3">点赞</label> <label class="caozuo4">${fsreply.key.good}个赞</label>
			</div>


<c:forEach items="${fsreply.value}" var="sreply">
			<div class="main_huifu_huifu">
				<!--  二级回复1-->

				<div class="main_huifu_content">
				${sreply.content}
				</div>

				<div class="main_hufuuser">
					<label class="zuozhe">作者:</label> <label class="username">	${sreply.username}</label>
					<label class="riqi">日期:</label> <label class="riqix">	<f:formatDate value="${sreply.date}" pattern="yyyy-MM-dd " />
						</label>
				</div>


				<div class="main_huifu_caozuo">
					<label class="caozuo1">	${sreply.floor}层</label> <label class="louzhucaozuo23">回复(92)</label>
					<label class="caozuo3">点赞</label> <label class="caozuo4">	${sreply.good}个赞</label>
				</div>

			</div>
			<!--end二级级回复  -->

		</c:forEach>

			<div class="morehuifu">更多100条回复</div>
		</div>
		
		
		<!--end一级回复  -->
	</c:forEach>
	
	



<!-- <h4 align="center">下滑获取更多</h4> -->
<!-- <h4 align="center">暂无更多回复</h4> -->
	
	</div>
	<!--end 总div  -->
	
	<div class="editor1">
	<script type="text/plain" id="myEditor" style="width:864px;height:140px;">
</script>
  <button id="fabiao1" style="width:866px;border: outset 3px ; " >发表</button>
</div>
<img src="resources/img/jindu1.gif" id="jindu"  style="position: fixed; left: 50%;margin-left: -35px;top: 50%;margin-top: -30px;display: none;"/>

</body>

</html>